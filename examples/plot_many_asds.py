"""
Gets all Hanford ASC error signal channels for pitch and yaw
with clever use of nu.find_channels() and nu.make_array_from_buffers().
nu.find_channels() returns a list of nds2 buffers for all matched channels.
nu.make_array_from_buffers() just grabs their names.
Plots the raw ASDs.

Craig Cahillane
Nov 5, 2019
"""
import nds2utils as nu

# Will find channels that match this glob on nds.ligo-wa.caltech.edu:31200
buffers_P = nu.find_channels("H1:ASC*P_IN1_DQ")
buffers_Y = nu.find_channels("H1:ASC*Y_IN1_DQ")

channels_P = nu.make_array_from_buffers(buffers_P)
channels_Y = nu.make_array_from_buffers(buffers_Y)
gps_start = 1257046000
gps_stop = 1257047000
binwidth = 0.1  # Hz, frequency vector spacing
overlap = 0.5  # FFT overlap ratio

host_server = "nds.ligo.caltech.edu"
port_number = 31200
allow_data_on_tape = "True"

dataDict_pit = nu.get_psds(
    channels_P,
    gps_start,
    gps_stop,
    binwidth,
    overlap,
    host_server=host_server,
    port_number=port_number,
    allow_data_on_tape=allow_data_on_tape,
)
dataDict_yaw = nu.get_psds(
    channels_Y,
    gps_start,
    gps_stop,
    binwidth,
    overlap,
    host_server=host_server,
    port_number=port_number,
    allow_data_on_tape=allow_data_on_tape,
)

import matplotlib.pyplot as plt

plt.ion()
fig1 = nu.plot_asds(
    dataDict_pit,
    title="Uncalibrated and logbinned ASC pitch error signals",
    logbin=True,
)
plt.ioff()
fig2 = nu.plot_asds(
    dataDict_yaw,
    title="Uncalibrated and logbinned ASC yaw error signals  ",
    logbin=True,
)
plt.show()
