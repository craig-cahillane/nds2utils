"""
Plots the calibrated TF and coherence
between the ISS and several IFO ports at the time of an injection.

Craig Cahillane
Nov 5, 2019
"""
import nds2utils as nu

channels = ["H1:CAL-DELTAL_EXTERNAL_DQ", "H1:PSL-ISS_SECONDLOOP_RIN_OUTER_OUT_DQ"]
gps_start = 1256622575
duration = 80  # s
gps_stop = gps_start + duration
binwidth = 0.1  # Hz, frequency vector spacing
overlap = 0.5  # FFT overlap ratio

host_server = "nds.ligo.caltech.edu"
port_number = 31200
allow_data_on_tape = "True"

dataDict = nu.get_csds(
    channels,
    gps_start,
    gps_stop,
    binwidth,
    overlap,
    host_server=host_server,
    port_number=port_number,
    allow_data_on_tape=allow_data_on_tape,
)

# Make the DARM calibration using zpk
zeros = [30, 30, 30, 30, 30, 30]
poles = [0.3, 0.3, 0.3, 0.3, 0.3, 0.3]
gain = 1.0
unitsB = "m"
dataDict = nu.calibrate_chan(
    dataDict,
    "H1:CAL-DELTAL_EXTERNAL_DQ",
    zeros=zeros,
    poles=poles,
    gain=gain,
    units=unitsB,
)

# Make ISS RIN calibration (the channel is already calibrated into RIN so we trick dataDict into thinking we've calibrated it)
unitsA = "RIN"
dataDict = nu.calibrate_chan(
    dataDict,
    "H1:PSL-ISS_SECONDLOOP_RIN_OUTER_OUT_DQ",
    zeros=[],
    poles=[],
    gain=1,
    units=unitsA,
)

unitsTF = "{}/{}".format(unitsB, unitsA)
import matplotlib.pyplot as plt

nu.plot_tf(
    dataDict,
    "H1:PSL-ISS_SECONDLOOP_RIN_OUTER_OUT_DQ",
    "H1:CAL-DELTAL_EXTERNAL_DQ",
    title="Raw DARM/ISS TF at the time of an intensity injection",
    logbin=True,
    units=unitsTF,
)
plt.show()
