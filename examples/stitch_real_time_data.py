"""
Gathers real time H1:CAL-DELTAL_EXTERNAL_DQ and H1:LSC-REFL_SERVO_ERR_OUT_DQ data time series.
Default host_server:port_number = nds.ligo-wa.caltech.edu:31200

Craig Cahillane
Nov 5, 2019
"""
import nds2utils as nu

# both of these channels must be of 'ONLINE' type in order to stream.
# Check using nu.find_channels('H1:CAL-DELTAL*', channel_type='ONLINE')
channels = ["H1:CAL-DELTAL_EXTERNAL_DQ", "H1:LSC-REFL_SERVO_ERR_OUT_DQ"]
duration = 10  # seconds

host_server = "nds.ligo-wa.caltech.edu"
port_number = 31200

# sets up nds2.connection, iterates over it each second, and returns a data_dict.
dataDict = nu.stitch_real_time_data(
    channels, duration, host_server=host_server, port_number=port_number
)

# this is where the data is stored
cal_deltal_external_data = dataDict["H1:CAL-DELTAL_EXTERNAL_DQ"]["data"]
# this is where the gps start time tag from nds2 is stored
gps_start = dataDict["H1:CAL-DELTAL_EXTERNAL_DQ"]["gpsStart"]

# Plot raw time series data
import matplotlib.pyplot as plt

nu.plot_raw_data(
    dataDict,
    seconds=duration,
    downsample=2**5,
    title="Data streamed starting at GPS = {}".format(gps_start),
)
plt.show()
