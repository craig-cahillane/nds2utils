name = "nds2utils"

from nds2utils.version import __version__

version = __version__

from nds2utils.data_utils import *
from nds2utils.plot_utils import *
